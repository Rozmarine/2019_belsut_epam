package Nonabstract_Figures.FilmFigures;

import Abstract_Figures.Triangle;
import Interfaces.Film;

public class FilmTriangle extends Abstract_Figures.Triangle implements Interfaces.Film {
    public FilmTriangle(double side1,double side2,double side3){
        super(side1,side2,side3);
    }
    public FilmTriangle(Film filmFigure){
        super((Abstract_Figures.Figure)filmFigure);
    }
}
