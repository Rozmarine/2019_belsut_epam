package Nonabstract_Figures.FilmFigures;

import Abstract_Figures.Rectangular;
import Interfaces.Film;

public class FilmRectangular extends Abstract_Figures.Rectangular implements Interfaces.Film {
    public FilmRectangular(double width,double height){
        super(width,height);
    }
    public FilmRectangular(Film filmFigure){
        super((Abstract_Figures.Figure)filmFigure);
    }
}
