package Nonabstract_Figures.FilmFigures;

import Abstract_Figures.Circle;
import Interfaces.Film;

public class FilmCircle extends Abstract_Figures.Circle implements Interfaces.Film {
   public FilmCircle(double radius){
       super(radius);
   }
    public FilmCircle(Film filmFigure){
        super((Abstract_Figures.Figure)filmFigure);
    }
}
