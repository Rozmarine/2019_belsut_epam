package Nonabstract_Figures.FilmFigures;

import Abstract_Figures.Square;
import Interfaces.Film;

public class FilmSquare extends Abstract_Figures.Square implements Interfaces.Film {
    public FilmSquare(double side){
        super(side);
    }
    public FilmSquare(Film filmFigure){
        super((Abstract_Figures.Figure)filmFigure);
    }
}
