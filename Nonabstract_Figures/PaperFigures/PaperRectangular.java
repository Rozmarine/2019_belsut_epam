package Nonabstract_Figures.PaperFigures;

import Abstract_Figures.Rectangular;
import Interfaces.Paper;

public class PaperRectangular extends Abstract_Figures.Rectangular implements Interfaces.Paper {
    public PaperRectangular(double width,double height){
        super(width,height);
    }
    public PaperRectangular(Paper paperFigure){
        super((Abstract_Figures.Figure)paperFigure);
    }
}
