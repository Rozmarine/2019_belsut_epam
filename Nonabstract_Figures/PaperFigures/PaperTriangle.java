package Nonabstract_Figures.PaperFigures;

import Abstract_Figures.Triangle;
import Interfaces.Paper;

public class PaperTriangle extends Abstract_Figures.Triangle implements Interfaces.Paper {
    public PaperTriangle(double side1,double side2,double side3){
        super(side1,side2,side3);
    }
    public PaperTriangle(Paper paperFigure){
        super((Abstract_Figures.Figure)paperFigure);
    }
}
