package Nonabstract_Figures.PaperFigures;

import Abstract_Figures.Circle;
import Interfaces.Paper;

public class PaperCircle extends Abstract_Figures.Circle implements Interfaces.Paper {
    public PaperCircle(double radius){
        super(radius);
    }
    public PaperCircle(Paper paperFigure){
        super((Abstract_Figures.Figure)paperFigure);
    }
}
