package Nonabstract_Figures.PaperFigures;

import Abstract_Figures.Square;
import Interfaces.Paper;

public class PaperSquare extends Abstract_Figures.Square implements Interfaces.Paper {
    public PaperSquare(double side){
        super(side);
    }
    public PaperSquare(Paper paperFigure){
        super((Abstract_Figures.Figure)paperFigure);
    }
}
