public class Word {

    public int wordLength(String word){   // each word knows its length
        return word.length();
    }
    public String wordChanges(StringBuilder word, int i){    // method for changing words
        switch(i){
            case 1:
                return word.reverse().toString();    // changing the order of letters in the word to the transverse
                // break
            case 2:
                return word.append('+').toString();   // adds a "+" at the end of the word
                // break
            case 3:
                return word.deleteCharAt(0).toString();  // deletes the first letter of the word
                // break
            case 4:
                return word.insert(1,"-").toString(); // inserts a "-" betwee the 2nd and the third letters of the word
                // break
            case 5:
                return word.replace(0,1, "ok" ).toString(); // replaces two first letters to "ok"
            default:
                return word.delete(0,(word.length()-1)).toString();  // deletes the word
        }


    }
}
