import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {

    // making text of String class from the input file
    public String inputText() throws IOException {
        FileReader file = new FileReader("inputText.txt"); // reading from the text file
        Scanner initialText=new Scanner(file);
        file.close();
        StringBuilder text=new StringBuilder(); // here the text will be written
        while (initialText.hasNextLine()) {
            text.append(initialText.nextLine());
        }
        return text.toString();  // initial text
    }
    // divide text into sentences
    public String[] divideText(String text){
        Sentence[] sentences=new Sentence[];
        Matcher matcher = Pattern.compile("([^.!?]+[.!?])").matcher(text);  // find sentences which end by . ? !
        int counter=0;
        while (matcher.find()) {
            sentences[counter]=new Sentence();
            sentences[counter]=matcher.group(1); // forming the array of sentencies
            ++counter;
        }
        return sentences;
    }

    //String[] textSentencies[];

    // task1: to delete in every word of question sentences every letter which equal to the firsrt letter of this word
    public StringBuilder[] task1(String wholeText){
        int i=0;
        int j,k=0;

        StringBuilder[] newQuestionSentence=new StringBuilder();
        StringBuilder[] sentencies=divideText(wholeText);
        while (i<sentencies.length){
            if (sentencies[i].contains[?]=true){  // choosing question sentences
                while (j<sentencies[i].divideSentenceIntoWords.length) {
                    // deleting letters equal to the first in every word
                    while (k<sentencies[i].divideSentenceIntoWords[j].length()) {
                        if (sentencies[i].divideSentenceIntoWords[j].charAt(k).equals(sentencies[i].divideSentenceIntoWords[j].charAt(0)))
                            sentencies[i].divideSentenceIntoWords[j].delete(k, k + 1);
                        else ++k;
                    }
                    newQuestionSentence[j].append(sentencies[i].divideSentenceIntoWords[j]);
                    newQuestionSentence[j].append(" ");
                    ++j;

                }

            }


        }
        return newQuestionSentence;
    }



    // task2: find words in the first sentence which are not repeated in the remaining text
    public String task2(String wholeText){
        int i=0;
        StringBuilder firstSentence=new StringBuilder();
        String partText;
        while (i<wholeText.length()){
            if (".".equals(wholeText.charAt(i))|"?".equals(wholeText.charAt(i))|"!".equals(wholeText.charAt(i))) {
                partText = wholeText.replace(firstSentence.toString(), ""); // text without first sentence
                break;
            }

            firstSentence.append(wholeText.charAt(i));
            ++i;
        }
        Sentence firstSentence=new Sentence;
        firstSentence.someSentence=wholetext.substring(0,i-1);
        firstSentence.sentenceContent=firstSentence.divideSentenceIntoWords(someSentence);
        int j=1;
        StringBuilder nonRepeatedWords=new StringBuilder();
        int[] wordCounter[firstSentence.sentenceContent.length];
        while (j<sentenceContent.length) {
            if (partText.contains(sentenceContent[j]) = false) {
                nonRepeatedWord.append(sentenceContent[j]);
            }
        }
        return nonRepeatedWords.toString();
    }
    // task3: to change the firsrt and the last words of the exclaiming sentence to each other
    public StringBuilder[] task3(String wholeText){
        int i=0;
        int j,k=0;

        StringBuilder[] newExclSentence=new StringBuilder();
        String[] sentencies=divideText(wholeText);
        while (i<sentencies.length){
            if (sentencies[i].contains[!]=true){  // choosing exclaimation sentences
               sentencies[i].trim();
               while (j<sentencies[i].length()) {
                   if (" ".equals(sentence[i].charAt(j))) ++k;
                   ++j;
               }
               changeWordsPlace(sentencies[i],1,k+1);
                    newQuestionSentence[j].append(sentencies[i].divideSentenceIntoWords[j]);
                    newQuestionSentence[j].append(" ");
                    ++j;

                }

            }


        }
        return newQuestionSentence;
    }


        /* task4: to cut substring from every ordinary sentence
                 the substring starts from symbol1 and ends at symbol2 and has the maximal length*/
        public String[] task4(String wholeText, char symbol1, char symbol2) {
            int i = 0; // counters i,j
            int j = 0;

            String[] newOrdinarySentence = new String(); // array of modified ordinary sentencies
            String[] sentencies = divideText(wholeText); // dividing text into sentencies
            while (i < sentencies.length) {
                if (sentencies[i].contains[.]=true){  // choosing ordinary sentences
                    newOrdinarySentence[j] = sentencies[i].cutSubstring(sentencies[i], symbol1, symbol2);
                    ++j;
                }
                ++i;
            }
            return newOrdinarySentence;
        }


}
