import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

    public String sentencePointsExeptions[]=new String [] {"т. д", "т. п","т. к.", "т. е.", "f. e"}; // not end of a sentence
    public String sentenceEndPoints[]=new String[] {".","?","!","...","?!","!?"}; // end of a sentence
    public String sentenceSigns[]=new String[] {"-",":",";",",","(",")"};  // signs in a sentence
    public String[] sentenceContent[];
    public String someSentence;

    // method for splitting a sentence into separate words;
    public String[] divideSentenceIntoWords(String sentence){
        // deleting all point and signs in the input sentence
        for (int j=1;j<sentenceEndPoints.length;++j) sentence.replace(sentenceEndPoints[j - 1], "");
        for (int i=1;i<sentenceEndPoints.length;++i) sentence.replace(sentenceSigns[i-1],"");

        sentence.trim();  // the input sentence now contains only words and spaces between them
        String dividingSign=" "; // the sentence will be divided by a space
        String[] words = sentence.split(dividingSign);
        return words;
    }

    // method for searching a particular word in a sentence;
    public boolean istheWordintheSentence(String sentence, String word){
        Boolean is=false;
        String[] allSentenceWords=divideSentenceIntoWords(sentence);
        for (int i=0;i<(allSentenceWords.length-1);++i) is=allSentenceWords[i].equals(word);
        return is;
    }

    // method for changing two words' places in a sentence;
    public String[] changeWordsPlace(String sentence, int firstWordIndex, int secondWordIndex) {
        String[] allSentenceWords = divideSentenceIntoWords(sentence);  // dividing into words
        String str = allSentenceWords[firstWordIndex - 1];
        allSentenceWords[firstWordIndex - 1] = allSentenceWords[secondWordIndex - 1]; // change word1 to word2
        allSentenceWords[secondWordIndex - 1] = str; // change word2 to word1
        return allSentenceWords;
    }

     // method fot reversing the words in the sentence
     public String wordsReverse(String inputSentence){
        StringBuilder reversedString=new StringBuilder();
        reversedString.append(inputSentence);
        return reversedString.reverse().toString();
      }

      // method for cutting substring(symbol1 - symbol2) of the maximal length from the sentence
    public String cutSubstring(String inputSentence,String symbol1,String symbol2){
        Pattern pattern = Pattern.compile(symbol1+symbol2);
        Matcher matcher = pattern.matcher(inputSentence);
        return inputSentence.replace((inputSentence.substring(matcher.start(), matcher.end())),"");
    }






}


