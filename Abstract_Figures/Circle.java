package Abstract_Figures;

// Abstract class Circle, which extends abstract class Figure.
// It is abstract because it can be made from paper and film, but the material can't be the field of the class.
public abstract class Circle extends Figure {
    private double radius;
    public Circle(double radius){
        this.radius=radius;
    }
    // Insert of getters and setters
    @Override
    public double getArea(){
        return Math.PI*Math.pow(radius,2);    // Gets the Area of circle
    }
    @Override
    public double getPerimeter() {
        return Math.PI * radius * 2;   // Gets the perimeter of circle
    }
    @Override
    public String toString(){
        return super.toString()+String.format(" radius=% 13f",radius);
    }

    // realization of cutting new figure from existing one
    public Circle(Figure figure){               // figure is a figure to cut from
        this.radius=figure.getMinSize()/2;
    }
    @Override
    public double getMinSize(){
        return radius/2;                       // it is a radius of new circle cut from some figure
    }
}
