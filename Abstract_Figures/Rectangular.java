package Abstract_Figures;

// Abstract class Rectangular, which extends abstract class Figure.
// It is abstract because it can be made from paper and film, but the material can't be the field of the class.
public abstract class Rectangular extends Figure {
    private double width;
    private double height;
    public Rectangular(double width,double height){
        this.width=width;
        this.height=height;
    }
    // Insert of getters and setters
    @Override
    public double getArea(){
        return width*height;    // Gets the Area of the rectangular
    }
    @Override
    public double getPerimeter() {
        return 2*(width+height);   // Gets the perimeter of the rectangular
    }
    @Override
    public String toString(){
        return super.toString()+String.format(" width=% 13f",width)+String.format(" height=% 13f",height);
    }

    // realization of cutting new figure from existing one
    public Rectangular(Figure figure){               // figure is a figure to cut from
        this.width=figure.getMinSize()/2;
        this.height=width;
    }
    @Override
    public double getMinSize(){
        return Math.min(width,height);                       // choose minimal value from width and height
    }
}