package Abstract_Figures;

public abstract class Figure {
    /*Calculation of the figure area
    *@return the area of the figure*/
    public abstract double getArea();

    /*Calculation of the figure perimeter
     *@return the value of the perimeter of the figure*/
    public abstract double getPerimeter();

    /*Calculation of the cut figure area size
     *@return the size of the area which was cut from the "parent" existing figure*/
    public abstract double getMinSize();

    /*Creation a String corresponding to the figure
     *@return String corresponding to the figure*/
    @Override
    public String toString(){
        return "\n"+this.getClass().getSimpleName();
    }

}
