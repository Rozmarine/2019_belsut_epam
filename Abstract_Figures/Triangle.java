package Abstract_Figures;

// Abstract class Triangle, which extends abstract class Figure.
// It is abstract because it can be made from paper and film, but the material can't be the field of the class.
public abstract class Triangle extends Figure {
    private double side1;
    private double side2;
    private double side3;
    public Triangle(double side1,double side2,double side3){
        this.side1=side1;
        this.side2=side2;
        this.side3=side3;
    }
    // Insert of getters and setters
    @Override
    public double getArea(){
        double halfPerimeter=0.5*(side1+side2+side3); // or 0.5*getPerimeter()
        // Gets the Area of the triangular
        return Math.sqrt(halfPerimeter*(halfPerimeter-side1)*(halfPerimeter-side2)*(halfPerimeter-side3));
    }
    @Override
    public double getPerimeter() {
        return side1+side2+side3;   // Gets the perimeter of the triangular
    }
    @Override
    public String toString(){
        return super.toString()+String.format(" side1=% 13f",side1)+String.format(" side2=% 13f",side2)+String.format(" side3=% 13f",side3);
    }

    // realization of cutting new figure from existing one
    public Triangle(Figure figure){               // figure is a figure to cut from, cut figure is a triangle
        this.side1=figure.getMinSize();
        this.side2=side1;
        this.side3=side1;
    }
    @Override
    public double getMinSize(){
        return Math.min(Math.min(side1,side2),Math.min(side2,side3));                       // choose minimal value from width and height
    }
}