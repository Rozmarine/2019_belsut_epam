package Abstract_Figures;

// Abstract class Square, which extends abstract class Figure.
// It is abstract because it can be made from paper and film, but the material can't be the field of the class.
public abstract class Square extends Figure {
    private double side;
    public Square(double side){
        this.side=side;
    }
    // Insert of getters and setters
    @Override
    public double getArea(){
        return side*side;    // Gets the Area of the square
    }
    @Override
    public double getPerimeter() {
        return 4*side;   // Gets the perimeter of the square
    }
    @Override
    public String toString(){
        return super.toString()+String.format(" side=% 13f",side);
    }

    // realization of cutting new figure from existing one
    public Square(Figure figure){               // figure is a figure to cut from, the new figure is square, it is cut from the figure
        this.side=figure.getMinSize()/2;
    }
    @Override
    public double getMinSize(){
        return side/2;                       // side of a new square
    }
}
